//
// Created by cpud36 on 18.11.16.
//

#ifndef BATTLESHIP_BUTTON_HPP
#define BATTLESHIP_BUTTON_HPP


#include <SFML/Graphics.hpp>

class Button : public sf::Drawable {
  sf::RectangleShape rect;
  sf::Text text;
public:
  Button(std::string txt);

  void setSize(const sf::Vector2f &size);

  const sf::Vector2f &getSize() { return rect.getSize(); }

  const sf::RectangleShape &getRect() const { return rect; }

  const sf::Text &getText() const { return text; }

  void setCenter(const sf::Vector2<float> vector2);

private:
  virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
};


#endif //BATTLESHIP_BUTTON_HPP
