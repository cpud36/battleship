//
// Created by cpud36 on 18.11.16.
//

#include <Constants.hpp>
#include "../include/Button.hpp"


Button::Button(std::string txt) {
  rect.setFillColor(BUTTON_FILL_COLOR);
  rect.setOutlineColor(BUTTON_OUTLINE_COLOR);
  rect.setOutlineThickness(BUTTON_OUTLINE_THICKNESS);
  text.setFont(FontMain);
  text.setColor(FONT_COLOR);
  text.setString(txt);
  text.setCharacterSize(20);
}

void Button::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  target.draw(rect, states);
  target.draw(text, states);
}

void Button::setSize(const sf::Vector2f &size) {
  const auto center = -rect.getOrigin() + rect.getSize() / 2.0f;
  rect.setSize(size);
  setCenter(center);
}

void Button::setCenter(const sf::Vector2f center) {
  const auto rectOrigin = center - rect.getSize() / 2.0f;
  const auto textBounds = text.getLocalBounds();
  const auto textOrigin = center - sf::Vector2f(textBounds.width, textBounds.height) / 2.0f;
  rect.setOrigin(-rectOrigin);
  text.setOrigin(-textOrigin);
}
