//
// Created by cpud36 on 11/18/16.
//

#include <SFML/Graphics/RenderWindow.hpp>
#include "Game.hpp"


void Game::handle_event(sf::Event &event) {
  switch (state) {
    case State::MainMenu:
      mainMenu.handle_event(event);
      break;
    case State::Game:
      gameRender.handle_event(event);
      break;
  }
}

void Game::update() {
  switch (state) {
    case State::MainMenu:
      mainMenu.update();
      break;
    case State::Game:
      gameRender.update();
      break;
  }
}

void Game::setStateGame(std::shared_ptr<PlayerController> player1, std::shared_ptr<PlayerController> player2) {
  state = State::Game;
  gameRender.SetPlayers(player1, player2);
}

void Game::setStateMenu() {
  state = State::MainMenu;
  gameRender.reset();
}







