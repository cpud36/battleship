//
// Created by cpud36 on 17.12.16.
//

#include <GameLogic.hpp>
#include "AILogic.hpp"

using std::make_tuple;
using std::get;

void AILogic::setGrid(Grid *grid) {
  AILogic::grid = grid;
}

bool AILogic::doTurn() {
  if (shots.empty()) {
    return fireRandomCell();
  } else {
    return checkShots();
  }
}

bool AILogic::fireRandomCell() {
  const auto cell = getRandomCell();
  return fire(cell);
}

bool AILogic::fire(std::tuple<unsigned int, unsigned int> cell) {
  lastX = get<0>(cell);
  lastY = get<1>(cell);
  if (grid->fire(lastX, lastY)) {
    shots.push(cell);
    return false;
  } else {
    return true;
  }
}

std::tuple<unsigned int, unsigned int> AILogic::getRandomCell() const {
  unsigned int x;
  unsigned int y;
  do {
    x = (unsigned int) (rand() % GRID_WIDTH);
    y = (unsigned int) (rand() % GRID_HEIGHT);
  } while (grid->grid[x][y].fired);
  return make_tuple(x, y);
}

unsigned int AILogic::getLastX() const {
  return lastX;
}

unsigned int AILogic::getLastY() const {
  return lastY;
}

bool AILogic::checkShots() {
  while (!shots.empty()) {
    const auto old_cell = shots.front();
    const auto cell = lookForCellAround(old_cell);
    if (cell != old_cell) {
      return fire(cell);
    } else {
      shots.pop();
    }
  }

  return fireRandomCell();
}

std::tuple<unsigned int, unsigned int> AILogic::lookForCellAround(const std::tuple<unsigned int, unsigned int> cell) {
  const auto deltas = {make_tuple(-1, 0), make_tuple(1, 0), make_tuple(0, 1), make_tuple(0, -1)};
  const unsigned int x = get<0>(cell);
  const unsigned int y = get<1>(cell);
  if (grid->isShipDestroyed(x, y)) {
    return cell;
  }

  if ((grid->isValidIndex(x + 1, y) && grid->grid[x + 1][y].destroyed()) ||
      (grid->isValidIndex(x - 1, y) && grid->grid[x - 1][y].destroyed())) {
    for (int i = x + 1; i < grid->grid.size(); ++i) {
      if (grid->grid[i][y].loosed()) {
        break;
      } else if (!grid->grid[i][y].fired) {
        return make_tuple(i, y);
      }
    }
    for (int i = x - 1; i >= 0; --i) {
      if (grid->grid[i][y].loosed()) {
        break;
      } else if (!grid->grid[i][y].fired) {
        return make_tuple(i, y);
      }
    }
  } else if ((grid->isValidIndex(x, y + 1) && grid->grid[x][y + 1].destroyed()) ||
             (grid->isValidIndex(x, y - 1) && grid->grid[x][y - 1].destroyed())) {
    for (int i = y + 1; i < grid->grid[0].size(); ++i) {
      if (grid->grid[x][i].loosed()) {
        break;
      } else if (!grid->grid[x][i].fired) {
        return make_tuple(x, i);
      }
    }
    for (int i = y - 1; i >= 0; --i) {
      if (grid->grid[x][i].loosed()) {
        break;
      } else if (!grid->grid[x][i].fired) {
        return make_tuple(x, i);
      }
    }
  }

  auto toFire = cell;
  int index = rand() % 4;
  for (auto &e: deltas) {
    const int i = x + get<0>(e);
    const int j = y + get<1>(e);
    if (grid->isValidIndex(i, j) && !grid->grid[i][j].fired) {
      toFire = make_tuple(i, j);
    }
    if (!--index) {
      break;
    }
  }

  return toFire;
}
