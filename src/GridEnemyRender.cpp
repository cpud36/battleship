//
// Created by cpud36 on 22.11.16.
//

#include "GridEnemyRender.hpp"
#include "HumanPlayer.hpp"

GridEnemyRender::GridEnemyRender(HumanPlayer *player, Grid *grid) : GridRender(player, grid) {
  for (auto &line: shipShapes) {
    for (auto &e: line) {
      e.setFillColor(SHIP_FIRED_COLOR);
    }
  }
}

void GridEnemyRender::handle_event(sf::Event &event) {
  if (player->state != PlayerState::Firing) {
    return;
  }

  if (event.type == sf::Event::MouseButtonPressed) {
    const auto viewPosition = view.getCenter() - view.getSize() / 2.0f;
    const auto rect = sf::FloatRect(-viewPosition.x, -viewPosition.y, size.x, size.y);
    const auto point = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);
    if (rect.contains(point)) {
      const auto position = getCellPosition(point);
      if (!grid->grid[position.x][position.y].fired && !grid->fire(position.x, position.y)) {
        player->endTurn();
      }
    }
  }

}

void GridEnemyRender::realDraw(sf::RenderTarget &target, sf::RenderStates states) const {
  GridRender::realDraw(target, states);
  for (int i = 0; i < shipShapes.size(); ++i) {
    for (int j = 0; j < shipShapes[0].size(); ++j) {
      if (grid->grid[i][j].destroyed()) {
        target.draw(shipShapes[i][j], states);
      }
    }
  }
}

void GridEnemyRender::updateSize() {
  GridRender::updateSize();
  const auto cellStep = sf::Vector2f(size.x / shipShapes.size(), size.y / shipShapes[0].size());
  const auto shipSize = 0.1f * std::sqrt(cellStep.x * cellStep.x + cellStep.y * cellStep.y);
  for (int i = 0; i < shipShapes.size(); ++i) {
    for (int j = 0; j < shipShapes[0].size(); ++j) {
      shipShapes[i][j].setRadius(shipSize);
      shipShapes[i][j].setOrigin(-cellStep.x * i - cellStep.x / 2 + shipSize,
                                 -cellStep.y * j - cellStep.y / 2 + shipSize);
    }
  }
}
