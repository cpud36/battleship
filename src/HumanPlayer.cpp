//
// Created by cpud36 on 20.11.16.
//

#include "HumanPlayer.hpp"

void HumanPlayer::handle_event(sf::Event &event) {
  if (state != PlayerState::Locked) {
    grid1Render.handle_event(event);
    grid2Render.handle_event(event);
  } else {
    if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Space) {
      state = PlayerState::Firing;
    }
  }
}

void HumanPlayer::update() {
  if (state != PlayerState::Locked) {
    grid1Render.update();
    window->draw(grid1Render);
    grid2Render.update();
    window->draw(grid2Render);
  } else {
    window->draw(lockScreen);
  }
}

HumanPlayer::HumanPlayer(PlayerTeam team) : PlayerController(team),
                                            grid1Render(this, myGrid),
                                            grid2Render(this, opponentGrid),
                                            lockScreen(team == PlayerTeam::First ? LOCK_FIRST_TEXT : LOCK_SECOND_TEXT) {
}

void HumanPlayer::resize(sf::Vector2f size) {
  const auto rect = sf::FloatRect(0, 0, size.x, size.y);
  const auto view = sf::View(rect);
  const auto gridSize = sf::Vector2f(size.x / 2, size.y);

  lockScreen.resize(size);

  auto myView = view;
  grid1Render.setView(myView);
  grid1Render.resize(gridSize);
  auto opponentView = view;
  opponentView.move(-gridSize.x, 0);
  grid2Render.setView(opponentView);
  grid2Render.resize(gridSize);
}

void HumanPlayer::setGrids(Grid *my, Grid *opponent) {
  PlayerController::setGrids(my, opponent);
  grid1Render.setGrid(myGrid);
  grid1Render.update();
  grid2Render.setGrid(opponentGrid);
  grid2Render.update();
}

void HumanPlayer::endTurn() {
  PlayerController::endTurn();
  state = PlayerState::Locked;
}