#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <Constants.hpp>
#include "Game.hpp"

using std::cerr;
using std::endl;

int main() {
  loadResources();
  srand((unsigned int) time(0));

  sf::RenderWindow window(sf::VideoMode(800, 600), "Battleship");
  window.setVerticalSyncEnabled(true);

  Game game(&window);

  while (window.isOpen()) {
    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type != sf::Event::Closed) {
        game.handle_event(event);
      } else {
        window.close();
      }
    }
    window.clear(BACKGROUND_COLOR);
    game.update();
    window.display();
  }
  return 0;
}