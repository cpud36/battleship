//
// Created by cpud36 on 11/18/16.
//

#include <SFML/Graphics/RectangleShape.hpp>
#include <Constants.hpp>
#include <HumanPlayer.hpp>
#include <ComputerPlayer.hpp>
#include "MainMenu.hpp"
#include "Game.hpp"

MainMenu::MainMenu(Game *game, sf::RenderTarget *window)
    : game(game), window(window), buttonPVP("Player vs Player"),
      buttonPVC("Player vs Computer") {
  if (!game || !window) return;
  const auto sizeu = window->getSize();
  resize(sf::Vector2f(sizeu.x, sizeu.y));
}

void MainMenu::handle_event(sf::Event &event) {
  sf::Vector2f position;
  switch (event.type) {
    case sf::Event::Resized:
      window->setView(sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height)));
      resize(sf::Vector2f(event.size.width, event.size.height));
      break;
    case sf::Event::MouseButtonPressed:
      position = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);
      if (buttonPVP.getRect().getGlobalBounds().contains(position)) {
        game->setStateGame(std::make_shared<HumanPlayer>(PlayerTeam::First),
                           std::make_shared<HumanPlayer>(PlayerTeam::Second));
      }
      if (buttonPVC.getRect().getGlobalBounds().contains(position)) {
        game->setStateGame(std::make_shared<HumanPlayer>(PlayerTeam::First),
                           std::make_shared<ComputerPlayer>(PlayerTeam::Second));
      }
      break;
    default:
      break;
  }
}

void MainMenu::update() {
  window->draw(buttonPVP);
  window->draw(buttonPVC);
}

void MainMenu::resize(sf::Vector2f size) {
  const auto buttonSize = size / 4.0f;
  const auto center = size / 2.0f;
  const auto buttonPVPCenter = center + sf::Vector2f(0, center.y / 2.0f);
  const auto buttonPVCCenter = center - sf::Vector2f(0, center.y / 2.0f);
  buttonPVP.setCenter(buttonPVPCenter);
  buttonPVP.setSize(buttonSize);
  buttonPVC.setCenter(buttonPVCCenter);
  buttonPVC.setSize(buttonSize);
}




