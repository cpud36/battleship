//
// Created by cpud36 on 03.12.16.
//

#include "GridGenerator.hpp"

bool GridGenerator::isValidIndex(unsigned int x, unsigned int y) const {
  return x >= 0 && x < grid.size() && y >= 0 && y < grid[0].size();
}

bool GridGenerator::generate() {
  unsigned int size;
  for (size = 1; size < toPlace.size(); ++size) {
    if (toPlace[size]) {
      break;
    }
  }
  if (size == toPlace.size()) {
    return true;
  }

  for (int i = 0; i < GENERATOR_TRIES_LIMIT; ++i) {
    unsigned int x = (unsigned int) (rand() % grid.size());
    unsigned int y = (unsigned int) (rand() % grid[0].size());
    bool horizontal = (bool) (rand() % 2);
    if (placeShip(x, y, size, horizontal)) {
      toPlace[size]--;
      if (generate()) {
        return true;
      } else {
        toPlace[size]++;
        setShip(x, y, size, horizontal, false);
      }
    }
  }
  return false;
}

bool GridGenerator::placeShip(unsigned int x, unsigned int y, unsigned int length, bool horizontal) {
  if (canPlaceShip(x, y, length, horizontal)) {
    setShip(x, y, length, horizontal);
  } else {
    return false;
  }
  return true;
}

void GridGenerator::setShip(unsigned int x, unsigned int y, unsigned int length, bool horizontal, bool value) {
  for (int i = 0; i < length; ++i) {
    if (horizontal) {
      grid[x + i][y] = value;
    } else {
      grid[x][y + i] = value;
    }
  }
}

bool GridGenerator::canPlaceShip(unsigned int x, unsigned int y, unsigned int length, bool horizontal) const {
  if (!isValidShip(x, y, length, horizontal)) {
    return false;
  }
  int xBound = horizontal ? length + 1 : 2;
  int yBound = horizontal ? 2 : length + 1;
  for (int i = -1; i < xBound; ++i) {
    for (int j = -1; j < yBound; ++j) {
      if (isValidIndex(x + i, y + j) && grid[x + i][y + j]) {
        return false;
      }
    }
  }
  return true;
}

bool GridGenerator::isValidShip(unsigned int x, unsigned int y, unsigned int length, bool horizontal) const {
  auto xBound = horizontal ? length : 1;
  auto yBound = horizontal ? 1 : length;
  return x + xBound <= grid.size() && y + yBound <= grid[0].size();
}
