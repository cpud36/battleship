//
// Created by cpud36 on 19.11.16.
//


#include <Grid.hpp>
#include <queue>
#include <set>
#include <GridGenerator.hpp>

void Grid::addShip(const int x, const int y, int value) {
  if (!value) {
    grid[x][y] = 0;
    return;
  }
  int c = 0;
  for (int i = -1; i < 2; ++i) {
    for (int j = -1; j < 2; ++j) {
      if (x + i >= 0 && x + i < grid.size() && y + j >= 0 && y + j < grid[0].size()) {
        if (grid[x + i][y + j].shipID) {
          if (i != 0 && j != 0) {
            return;
          }
          c++;
        }
      }
    }
  }
  if (c > 1) {
    return;
  }

  grid[x][y].shipID = value;
}

bool Grid::validate() {
  assignShipIDs();
  return validateShipCount();
}

bool Grid::validateShipCount() {
  std::vector<int> shipSizes(largestID + 1, 0);
  for (int i = 0; i < grid.size(); ++i) {
    for (int j = 0; j < grid[0].size(); ++j) {
      if (grid[i][j].shipID == SHIPID_UNKNOWN) {
        continue;
      }
      shipSizes[grid[i][j].shipID]++;
    }
  }
  std::array<int, SHIP_SIZE_COUNT> sizes = {0};
  for (auto e = shipSizes.begin() + 1; e != shipSizes.end(); ++e) {
    if (*e >= SHIP_SIZE_COUNT) {
      return false;
    }
    sizes[*e]++;
  }

  for (int i = 1; i < SHIP_SIZE_COUNT; ++i) {
    if (sizes[i] != SHIP_COUNTS[i]) {
      return false;
    }
  }
  return true;
}

void Grid::assignShipIDs() {
  for (auto &line: grid) {
    for (auto &e: line) {
      if (e.shipID) {
        e.shipID = SHIPID_UNKNOWN;
      }
    }
  }
  largestID = 0;
  for (unsigned int x = 0; x < grid.size(); ++x) {
    for (unsigned int y = 0; y < grid[0].size(); ++y) {
      if (grid[x][y].shipID == SHIPID_UNKNOWN) {
        grid[x][y].shipID = suggestID(x, y);
      }
    }
  }
}

int Grid::suggestID(unsigned int x, unsigned int y) {
  int id = SHIPID_UNKNOWN;
  for (int i = -1; i < 2; ++i) {
    for (int j = -1; j < 2; ++j) {
      if (!isValidIndex(x + i, y + j)) {
        continue;
      }

      if (grid[x + i][y + j].shipID && grid[x + i][y + j].shipID != SHIPID_UNKNOWN) {
        id = grid[x + i][y + j].shipID;
      }
    }
  }
  if (id == SHIPID_UNKNOWN) {
    id = ++largestID;
  }
  return id;
}

bool Grid::isValidIndex(int x, int y) const {
  return x >= 0 && x < grid.size() && y >= 0 && y < grid[0].size();
}

bool Grid::fire(unsigned int x, unsigned int y) {
  grid[x][y].fired = true;
  if (grid[x][y].destroyed()) {
    checkShipDestroyed(x, y);
    destroyed = checkIsDestroyed();
  }
  return grid[x][y].destroyed();
}

bool Grid::checkIsDestroyed() {
  for (auto e: shipDestroyed) {
    if (!e) {
      return false;
    }
  }
  return true;
}

template<class T>
struct SetComparator {
  bool operator()(const sf::Vector2<T> &a, const sf::Vector2<T> &b) const {
    if (a.x != b.x) {
      return a.x < b.x;
    }
    return a.y < b.y;
  }
};

void Grid::checkShipDestroyed(unsigned int x, unsigned int y) {
  //TODO: split this into several functions
  const auto id = grid[x][y].shipID;
  std::queue<sf::Vector2u> shipQ;
  std::queue<sf::Vector2u> fillQ;
  std::set<sf::Vector2u, SetComparator<unsigned int>> visited;
  shipQ.push(sf::Vector2u(x, y));
  fillQ.push(sf::Vector2u(x, y));
  visited.insert(sf::Vector2u(x, y));
  while (!shipQ.empty()) {
    auto c = shipQ.front();
    shipQ.pop();
    if (grid[c.x][c.y].shipID != id) {
      continue;
    }

    if (!grid[c.x][c.y].destroyed()) {
      return;
    }

    if (isValidIndex(c.x + 1, c.y) && grid[c.x + 1][c.y].shipID == id &&
        visited.find(sf::Vector2u(c.x + 1, c.y)) == visited.end()) {
      shipQ.push(sf::Vector2u(c.x + 1, c.y));
      fillQ.push(sf::Vector2u(c.x + 1, c.y));
      visited.insert(sf::Vector2u(c.x + 1, c.y));
    }
    if (isValidIndex(c.x - 1, c.y) && grid[c.x - 1][c.y].shipID == id &&
        visited.find(sf::Vector2u(c.x - 1, c.y)) == visited.end()) {
      shipQ.push(sf::Vector2u(c.x - 1, c.y));
      fillQ.push(sf::Vector2u(c.x - 1, c.y));
      visited.insert(sf::Vector2u(c.x - 1, c.y));
    }
    if (isValidIndex(c.x, c.y + 1) && grid[c.x][c.y + 1].shipID == id &&
        visited.find(sf::Vector2u(c.x, c.y + 1)) == visited.end()) {
      shipQ.push(sf::Vector2u(c.x, c.y + 1));
      fillQ.push(sf::Vector2u(c.x, c.y + 1));
      visited.insert(sf::Vector2u(c.x, c.y + 1));
    }
    if (isValidIndex(c.x, c.y - 1) && grid[c.x][c.y - 1].shipID == id &&
        visited.find(sf::Vector2u(c.x, c.y - 1)) == visited.end()) {
      shipQ.push(sf::Vector2u(c.x, c.y - 1));
      fillQ.push(sf::Vector2u(c.x, c.y - 1));
      visited.insert(sf::Vector2u(c.x, c.y - 1));
    }
  }

  shipDestroyed[id] = true;
  while (!fillQ.empty()) {
    auto c = fillQ.front();
    fillQ.pop();
    for (int i = -1; i < 2; ++i) {
      for (int j = -1; j < 2; ++j) {
        if (!isValidIndex(c.x + i, c.y + j)) {
          continue;
        }
        grid[c.x + i][c.y + j].fired = true;
      }
    }
  }
}

void Grid::generate() {
  GridGenerator generator;
  generator.generate();
  for (int i = 0; i < grid.size(); ++i) {
    for (int j = 0; j < grid[0].size(); ++j) {
      grid[i][j] = CellState(generator.grid[i][j] ? SHIPID_UNKNOWN : 0);
    }
  }
  assignShipIDs();
}

bool Grid::isShipDestroyed(const unsigned int x, const unsigned int y) const {
  return shipDestroyed[grid[x][y].shipID - 1];
}

void Grid::reset() {
  for (auto &line: grid) {
    for (auto &e: line) {
      e = CellState();
    }
  }
}




