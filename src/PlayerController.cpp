//
// Created by cpud36 on 20.11.16.
//
#include "PlayerController.hpp"

void PlayerController::setRenderTarget(sf::RenderTarget *window) {
  PlayerController::window = window;
  resize(window->getView().getSize());
}

void PlayerController::setGrids(Grid *my, Grid *opponent) {
  myGrid = my;
  opponentGrid = opponent;
}

void PlayerController::setGameLogic(GameLogic *gameLogic) {
  PlayerController::gameLogic = gameLogic;
}

void PlayerController::endTurn() {
  gameLogic->setTurn(team == PlayerTeam::First ? PlayerTeam::Second : PlayerTeam::First);
}

