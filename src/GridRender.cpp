//
// Created by cpud36 on 20.11.16.
//

#include "GridShipRender.hpp"
#include "HumanPlayer.hpp"

GridRender::GridRender(HumanPlayer *player, Grid *grid) : grid(grid), player(player) {
  outline.setOrigin(0, 0);
  outline.setFillColor(sf::Color::Transparent);
  outline.setOutlineColor(GRID_OUTLINE_COLOR);
  outline.setOutlineThickness(GRID_OUTLINE_THICKNESS);
  updateSize();
  for (int i = 0; i < gridShapes.size(); ++i) {
    for (int j = 0; j < gridShapes[0].size(); ++j) {
      gridShapes[i][j].setOutlineColor(GRID_CROSS_COLOR);
      gridShapes[i][j].setOutlineThickness(GRID_CROSS_THICKNESS);
    }
  }
  if (grid) {
    update();
  }
}

void GridRender::update() {
  for (int i = 0; i < gridShapes.size(); ++i) {
    for (int j = 0; j < gridShapes[0].size(); ++j) {
      if (grid->grid[i][j].fired) {
        gridShapes[i][j].setFillColor(GRID_FIRED_COLOR);
      } else {
        gridShapes[i][j].setFillColor(GRID_UNFIRED_COLOR);
      }
    }
  }
}

void GridRender::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  const auto prev_view = target.getView();
  target.setView(view);
  realDraw(target, states);
  target.setView(prev_view);
}

void GridRender::realDraw(sf::RenderTarget &target, sf::RenderStates states) const {
  for (int i = 0; i < gridShapes.size(); ++i) {
    for (int j = 0; j < gridShapes[0].size(); ++j) {
      target.draw(gridShapes[i][j], states);
    }
  }
  target.draw(outline, states);
}

void GridRender::updateSize() {
  const auto cellSize = sf::Vector2f(size.x / gridShapes.size(), size.y / gridShapes[0].size());
  const static auto outlineThickness = sf::Vector2f(GRID_OUTLINE_THICKNESS, GRID_OUTLINE_THICKNESS);
  const static auto crossThickness = sf::Vector2f(GRID_CROSS_THICKNESS, GRID_CROSS_THICKNESS);
  outline.setSize(size - 2.0f * outlineThickness);
  for (int i = 0; i < gridShapes.size(); ++i) {
    for (int j = 0; j < gridShapes[0].size(); ++j) {
      gridShapes[i][j].setSize(cellSize - 2.0f * crossThickness);
      gridShapes[i][j].setOrigin(-cellSize.x * i + crossThickness.x, -cellSize.y * j + crossThickness.y);
    }
  }
}

void GridRender::resize(const sf::Vector2f &size) {
  GridRender::size = size;
  updateSize();
}


void GridRender::setView(const sf::View &view) {
  GridRender::view = view;
}

sf::Vector2u GridRender::getCellPosition(const sf::Vector2f &point) const {
  const auto cellStep = sf::Vector2f(size.x / gridShapes.size(), size.y / gridShapes.size());
  const auto viewRect = getViewRect();
  const unsigned int x = static_cast<unsigned int>(floor((point.x + viewRect.left) / cellStep.x));
  const unsigned int y = static_cast<unsigned int>(floor((point.y + viewRect.top) / cellStep.y));
  return sf::Vector2u(x, y);
}

const sf::Rect<float> GridRender::getViewRect() const {
  const auto viewPosition = view.getCenter() - view.getSize() / 2.0f;
  const auto rect = sf::FloatRect(viewPosition.x, viewPosition.y, size.x, size.y);
  return rect;
}