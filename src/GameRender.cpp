//
// Created by cpud36 on 11/18/16.
//

#include "GameRender.hpp"
#include "Game.hpp"

GameRender::GameRender(Game *game, sf::RenderTarget *window) : game(game), window(window),
                                                               win1Screen(WIN_FIRST_TEXT),
                                                               win2Screen(WIN_SECOND_TEXT) {
  const auto size = window->getView().getSize();
  win1Screen.resize(size);
  win2Screen.resize(size);
}

void GameRender::handle_event(sf::Event &event) {
  if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
    game->setStateMenu();
    return;
  }
  if (event.type == sf::Event::Resized) {
    window->setView(sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height)));
    const auto &size = sf::Vector2f(event.size.width, event.size.height);
    player1->resize(size);
    player2->resize(size);
    win1Screen.resize(size);
    win2Screen.resize(size);
  }

  if (gameLogic.getTurn() == PlayerTeam::First) {
    player1->handle_event(event);
  } else {
    player2->handle_event(event);
  }
}

void GameRender::update() {
  if (gameLogic.isOver()) {
    if (gameLogic.getWinner() == PlayerTeam::First) {
      window->draw(win1Screen);
    } else {
      window->draw(win2Screen);
    }
  } else if (gameLogic.getTurn() == PlayerTeam::First) {
    player1->update();
  } else {
    player2->update();
  }
}


void GameRender::SetPlayers(std::shared_ptr<PlayerController> p1, std::shared_ptr<PlayerController> p2) {
  player1 = p1;
  player2 = p2;
  player1->setGrids(&gameLogic.grid1, &gameLogic.grid2);
  player1->setRenderTarget(window);
  player1->setGameLogic(&gameLogic);
  player2->setGrids(&gameLogic.grid2, &gameLogic.grid1);
  player2->setRenderTarget(window);
  player2->setGameLogic(&gameLogic);
}

void GameRender::reset() {
  gameLogic.resetGrids();
}
