//
// Created by cpud36 on 17.12.16.
//

#include <iostream>
#include "ComputerPlayer.hpp"

ComputerPlayer::ComputerPlayer(PlayerTeam team) : PlayerController(team),
                                                  screen(
                                                      team == PlayerTeam::First ? LOCK_FIRST_TEXT : LOCK_SECOND_TEXT),
                                                  bellowText(BELLOW_COMPUTER_TEXT, FontMain, BELLOW_FONT_SIZE),
                                                  start_time(std::chrono::steady_clock::now()) {
}

void ComputerPlayer::handle_event(sf::Event &event) {
}

void ComputerPlayer::update() {
  if (!built) {
    built = true;
    endTurn();
    return;
  }

  doTurn();

  window->draw(screen);
  window->draw(bellowText);
}

void ComputerPlayer::resize(sf::Vector2f size) {
  ComputerPlayer::size = size;
  screen.resize(size);
  updateTextPosition();
}

void ComputerPlayer::updateTextPosition() {
  const auto textBounds = bellowText.getLocalBounds();
  const auto textSize = sf::Vector2f(textBounds.width, textBounds.height);
  const auto center = size / 2.0f;
  const auto textCenter = center + sf::Vector2f(0, center.y / 2.0f);
  const auto textOrigin = textCenter - textSize / 2.0f;
  bellowText.setOrigin(-textOrigin);
}

void ComputerPlayer::setGrids(Grid *my, Grid *opponent) {
  PlayerController::setGrids(my, opponent);
  logic.setGrid(opponent);
  my->generate();
}

void ComputerPlayer::endTurn() {
  PlayerController::endTurn();
  finished = false;
  start_time = std::chrono::steady_clock::now();
  firedThisTurn = false;
  bellowStr = BELLOW_COMPUTER_TEXT;
  setTextString(bellowStr);
}

void ComputerPlayer::doTurn() {
  const auto now = std::chrono::steady_clock::now();
  const auto t1 = start_time + COMPUTER_THINK_TIME;
  if (now >= start_time + COMPUTER_THINK_TIME) {
    start_time = now;
    if (finished) {
      endTurn();
    } else {
      finished = logic.doTurn();
      std::string txt = "a1";
      txt[0] = 'a' + (unsigned char) logic.getLastX();
      if (logic.getLastY() >= 9) {
        txt += '0' + (unsigned char) logic.getLastY() - 9;
      } else {
        txt[1] = '1' + (unsigned char) logic.getLastY();
      }
      if (firedThisTurn) {
        bellowStr += ", ";
      } else {
        firedThisTurn = true;
      }
      bellowStr += txt;
      setTextString(bellowStr);
    }
  }
}

void ComputerPlayer::setTextString(std::string &str) {
  bellowText.setString(str);
  updateTextPosition();
}
