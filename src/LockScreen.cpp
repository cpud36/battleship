//
// Created by cpud36 on 24.11.16.
//

#include "LockScreen.hpp"

void LockScreen::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  target.clear(LOCK_BACKGROUND_COLOR);
  target.draw(text, states);

}

void LockScreen::resize(sf::Vector2f size) {
  const auto center = size / 2.0f;
  const auto bounds = text.getLocalBounds();
  const auto textSize = sf::Vector2f(bounds.width, bounds.height);
  const auto origin = center - textSize / 2.0f;
  text.setOrigin(-origin);
}

