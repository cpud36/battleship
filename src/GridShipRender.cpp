//
// Created by cpud36 on 20.11.16.
//

#include "GridShipRender.hpp"
#include "HumanPlayer.hpp"

GridShipRender::GridShipRender(HumanPlayer *player, Grid *grid) : GridRender(player, grid) {
}

void GridShipRender::handle_event(sf::Event &event) {
  if (player->state != PlayerState::Building) {
    return;
  }
  if (event.type == sf::Event::KeyPressed) {
    switch (event.key.code) {
      case sf::Keyboard::Space:
        if (grid->validate()) {
          player->endTurn();
        }
        break;
      case sf::Keyboard::R:
        grid->generate();
        break;
      default:
        break;
    }
  }
  if (event.type == sf::Event::MouseButtonPressed) {
    const sf::Rect<float> rect = getViewRect();
    const auto point = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);
    if (rect.contains(point)) {
      const auto position = getCellPosition(point);
      grid->addShip(position.x, position.y, event.mouseButton.button == sf::Mouse::Left);
    }
  }
}

void GridShipRender::realDraw(sf::RenderTarget &target, sf::RenderStates states) const {
  GridRender::realDraw(target, states);
  for (int i = 0; i < shipShapes.size(); ++i) {
    for (int j = 0; j < shipShapes[0].size(); ++j) {
      if (grid->grid[i][j].shipID) {
        target.draw(shipShapes[i][j], states);
      }
    }
  }
}

void GridShipRender::update() {
  GridRender::update();
  for (int i = 0; i < shipShapes.size(); ++i) {
    for (int j = 0; j < shipShapes[0].size(); ++j) {
      if (grid->grid[i][j].fired) {
        shipShapes[i][j].setFillColor(SHIP_FIRED_COLOR);
      } else {
        shipShapes[i][j].setFillColor(SHIP_UNFIRED_COLOR);
      }
    }
  }
}

void GridShipRender::updateSize() {
  GridRender::updateSize();
  const auto cellStep = sf::Vector2f(size.x / shipShapes.size(), size.y / shipShapes[0].size());
  const auto shipSize = 0.1f * std::sqrt(cellStep.x * cellStep.x + cellStep.y * cellStep.y);
  for (int i = 0; i < shipShapes.size(); ++i) {
    for (int j = 0; j < shipShapes[0].size(); ++j) {
      shipShapes[i][j].setRadius(shipSize);
      shipShapes[i][j].setOrigin(-cellStep.x * i - cellStep.x / 2 + shipSize,
                                 -cellStep.y * j - cellStep.y / 2 + shipSize);
    }
  }
}

