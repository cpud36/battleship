//
// Created by cpud36 on 11/18/16.
//

#ifndef BATTLESHIP_SCENE_HPP
#define BATTLESHIP_SCENE_HPP

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <memory>
#include "MainMenu.hpp"
#include "State.hpp"
#include "GameRender.hpp"
#include "PlayerController.hpp"

class Game {
  sf::RenderTarget *window;
  MainMenu mainMenu;
  GameRender gameRender;
  State state = State::MainMenu;
public:
  Game(sf::RenderTarget *window) : window(window), mainMenu(this, window), gameRender(this, window) { }

  void setStateGame(std::shared_ptr<PlayerController> player1, std::shared_ptr<PlayerController> player2);

  void setStateMenu();

  void handle_event(sf::Event &event);

  void update();
};


#endif //BATTLESHIP_SCENE_HPP
