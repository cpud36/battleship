//
// Created by cpud36 on 17.12.16.
//

#ifndef BATTLESHIP_COMPUTERPLAYER_HPP
#define BATTLESHIP_COMPUTERPLAYER_HPP


#include "PlayerController.hpp"
#include "LockScreen.hpp"
#include "AILogic.hpp"
#include <chrono>

class ComputerPlayer : public PlayerController {
  LockScreen screen;
  sf::Text bellowText;
  std::chrono::steady_clock::time_point start_time;
  AILogic logic;
  bool finished = false;
  bool built = false;
  bool firedThisTurn = false;
  sf::Vector2f size;

  std::string bellowStr = BELLOW_COMPUTER_TEXT;

  void doTurn();

  void updateTextPosition();

  void setTextString(std::string &str);

public:
  ComputerPlayer(PlayerTeam team);

  void handle_event(sf::Event &event) override;

  void update() override;

  void resize(sf::Vector2f size) override;

  void setGrids(Grid *my, Grid *opponent) override;

  void endTurn() override;
};


#endif //BATTLESHIP_COMPUTERPLAYER_HPP
