//
// Created by cpud36 on 20.11.16.
//

#ifndef BATTLESHIP_GRIDRENDER_HPP
#define BATTLESHIP_GRIDRENDER_HPP


#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "Constants.hpp"
#include "Grid.hpp"

class HumanPlayer;

class GridRender : public sf::Drawable {
  std::array<std::array<sf::RectangleShape, GRID_WIDTH>, GRID_HEIGHT> gridShapes;
  sf::RectangleShape outline;
protected:
  sf::View view;
  Grid *grid;
  HumanPlayer *player;
  sf::Vector2f size;

  sf::Vector2u getCellPosition(const sf::Vector2f &point) const;

  const sf::Rect<float> getViewRect() const;

public:
  GridRender(HumanPlayer *player, Grid *grid);

  void setGrid(Grid *grid) { GridRender::grid = grid; }

  virtual void handle_event(sf::Event &event) = 0;

  void setView(const sf::View &view);

  virtual void resize(const sf::Vector2f &size);

  virtual void update();

protected:
  virtual void updateSize();

  virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;

  virtual void realDraw(sf::RenderTarget &target, sf::RenderStates states) const;
};


#endif //BATTLESHIP_GRIDRENDER_HPP
