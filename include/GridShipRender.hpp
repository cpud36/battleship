//
// Created by cpud36 on 20.11.16.
//

#ifndef BATTLESHIP_GRIDSHIPRENDER_HPP
#define BATTLESHIP_GRIDSHIPRENDER_HPP


#include "GridRender.hpp"

class GridShipRender : public GridRender {
  std::array<std::array<sf::CircleShape, GRID_WIDTH>, GRID_HEIGHT> shipShapes;
public:
  GridShipRender(HumanPlayer *player, Grid *grid);

  virtual void handle_event(sf::Event &event);

  virtual void update();

protected:
  virtual void realDraw(sf::RenderTarget &target, sf::RenderStates states) const;

  virtual void updateSize();

};


#endif //BATTLESHIP_GRIDSHIPRENDER_HPP
