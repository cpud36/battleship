//
// Created by cpud36 on 22.11.16.
//

#ifndef BATTLESHIP_GRIDENEMYRENDER_HPP
#define BATTLESHIP_GRIDENEMYRENDER_HPP


#include "GridRender.hpp"

class GridEnemyRender : public GridRender {
  std::array<std::array<sf::CircleShape, GRID_WIDTH>, GRID_HEIGHT> shipShapes;
public:
  GridEnemyRender(HumanPlayer *player, Grid *grid);

  virtual void handle_event(sf::Event &event);

protected:
  void realDraw(sf::RenderTarget &target, sf::RenderStates states) const override;

  void updateSize() override;

};


#endif //BATTLESHIP_GRIDENEMYRENDER_HPP
