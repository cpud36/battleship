//
// Created by cpud36 on 23.11.16.
//

#ifndef BATTLESHIP_PLAYERSTATE_HPP
#define BATTLESHIP_PLAYERSTATE_HPP


enum class PlayerState {
  Building,
  Locked,
  Firing
};


#endif //BATTLESHIP_PLAYERSTATE_HPP
