//
// Created by cpud36 on 24.11.16.
//

#ifndef BATTLESHIP_LOCKSCREEN_HPP
#define BATTLESHIP_LOCKSCREEN_HPP


#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Text.hpp>
#include "Constants.hpp"

class LockScreen : public sf::Drawable {
  sf::Text text;
public:
  LockScreen(const std::string &text) : text(text, FontMain, LOCK_FONT_SIZE) { }

  void resize(sf::Vector2f size);

protected:
  virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;


};


#endif //BATTLESHIP_LOCKSCREEN_HPP
