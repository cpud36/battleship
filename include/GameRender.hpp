//
// Created by cpud36 on 11/18/16.
//

#ifndef BATTLESHIP_GAMERENDER_HPP
#define BATTLESHIP_GAMERENDER_HPP

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <memory>
#include "State.hpp"
#include "PlayerController.hpp"
#include "GameLogic.hpp"
#include "LockScreen.hpp"

class Game;

class GameRender {
  Game *game;
  sf::RenderTarget *window;
  std::shared_ptr<PlayerController> player1 = nullptr;
  std::shared_ptr<PlayerController> player2 = nullptr;
  GameLogic gameLogic;
  LockScreen win1Screen;
  LockScreen win2Screen;
public:
  GameRender(Game *game = nullptr, sf::RenderTarget *window = nullptr);

  void handle_event(sf::Event &event);

  void update();

  void SetPlayers(std::shared_ptr<PlayerController> p1, std::shared_ptr<PlayerController> p2);

  void reset();
};


#endif //BATTLESHIP_GAMERENDER_HPP
