//
// Created by cpud36 on 17.12.16.
//

#ifndef BATTLESHIP_AILOGIC_HPP
#define BATTLESHIP_AILOGIC_HPP


#include <queue>
#include "Grid.hpp"

class AILogic {
  Grid *grid = nullptr;
  unsigned int lastX;
  unsigned int lastY;
  std::queue<std::tuple<unsigned int, unsigned int>> shots;

  std::tuple<unsigned int, unsigned int> getRandomCell() const;

  bool fireRandomCell();

  bool fire(std::tuple<unsigned int, unsigned int> cell);

public:
  void setGrid(Grid *grid);

  bool doTurn();

  unsigned int getLastX() const;

  unsigned int getLastY() const;

  bool checkShots();

  std::tuple<unsigned int, unsigned int> lookForCellAround(const std::tuple<unsigned int, unsigned int> tuple);
};


#endif //BATTLESHIP_AILOGIC_HPP
