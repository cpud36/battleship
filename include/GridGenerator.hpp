//
// Created by cpud36 on 03.12.16.
//

#ifndef BATTLESHIP_GRIDGENERATOR_HPP
#define BATTLESHIP_GRIDGENERATOR_HPP


#include "CellState.hpp"
#include "Constants.hpp"

class GridGenerator {
  std::array<unsigned int, SHIP_SIZE_COUNT> toPlace = SHIP_COUNTS;

  bool placeShip(unsigned int x, unsigned int y, unsigned int length, bool horizontal);

  bool isValidIndex(unsigned int x, unsigned int y) const;

  bool canPlaceShip(unsigned int x, unsigned int y, unsigned int length, bool horizontal) const;

  bool isValidShip(unsigned int x, unsigned int y, unsigned int length, bool horizontal) const;

  void setShip(unsigned int x, unsigned int y, unsigned int length, bool horizontal, bool value = true);

public:
  std::array<std::array<bool, GRID_WIDTH>, GRID_HEIGHT> grid = {{{{false}}}};

  bool generate();
};


#endif //BATTLESHIP_GRIDGENERATOR_HPP
