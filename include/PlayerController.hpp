//
// Created by cpud36 on 20.11.16.
//

#ifndef BATTLESHIP_PLAYERCONTROLLER_HPP
#define BATTLESHIP_PLAYERCONTROLLER_HPP


#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "PlayerTeam.hpp"
#include "Grid.hpp"
#include "GameLogic.hpp"

class PlayerController {
protected:
  PlayerTeam team;
  sf::RenderTarget *window = nullptr;
  Grid *myGrid = nullptr;
  Grid *opponentGrid = nullptr;
  GameLogic *gameLogic = nullptr;
public:
  PlayerController(PlayerTeam team) : team(team) { }

  virtual void setRenderTarget(sf::RenderTarget *window);

  virtual void setGrids(Grid *my, Grid *opponent);

  void setGameLogic(GameLogic *gameLogic);

  virtual void handle_event(sf::Event &event) = 0;

  virtual void update() = 0;

  virtual void resize(sf::Vector2f size) = 0;

  virtual void endTurn();
};


#endif //BATTLESHIP_PLAYERCONTROLLER_HPP
