//
// Created by cpud36 on 11/18/16.
//

#ifndef BATTLESHIP_CONSTANTS_HPP
#define BATTLESHIP_CONSTANTS_HPP

#include <SFML/Graphics.hpp>
#include <chrono>

using std::chrono::seconds;

const auto BACKGROUND_COLOR = sf::Color(200, 200, 255);

const auto BUTTON_FILL_COLOR = sf::Color(255, 255, 255);
const auto BUTTON_OUTLINE_COLOR = sf::Color(150, 150, 150);
const auto BUTTON_OUTLINE_THICKNESS = 1;

extern sf::Font FontArial;
extern sf::Font &FontMain;
const auto FONT_COLOR = sf::Color(0, 0, 0);

const auto GRID_WIDTH = 10;
const auto GRID_HEIGHT = 10;
const auto GRID_OUTLINE_COLOR = sf::Color(50, 50, 50);
const auto GRID_OUTLINE_THICKNESS = 2;
const auto GRID_CROSS_COLOR = sf::Color(150, 150, 150);
const auto GRID_CROSS_THICKNESS = 1;
const auto GRID_UNFIRED_COLOR = sf::Color(200, 200, 200);
const auto GRID_FIRED_COLOR = sf::Color(255, 100, 100);
const auto SHIP_UNFIRED_COLOR = sf::Color(100, 255, 100);
const auto SHIP_FIRED_COLOR = sf::Color(100, 100, 100);

const auto LOCK_BACKGROUND_COLOR = sf::Color(50, 50, 50);
const auto LOCK_FONT_SIZE = 80;
const auto LOCK_FIRST_TEXT = "Player 1";
const auto LOCK_SECOND_TEXT = "Player 2";

const auto BELLOW_FONT_SIZE = 36;
const std::chrono::steady_clock::duration COMPUTER_THINK_TIME = seconds(1);
const auto BELLOW_COMPUTER_TEXT = "is thinking... He's already shot ";

const auto WIN_FIRST_TEXT = "Player 1 won";
const auto WIN_SECOND_TEXT = "Player 2 won";

const auto SHIP_SIZE_COUNT = 6;
const std::array<unsigned int, SHIP_SIZE_COUNT> SHIP_COUNTS = {0, 2, 0, 1, 1, 1};
const auto TOTAL_SHIP_COUNT = 5;
const auto GENERATOR_TRIES_LIMIT = 20;

void loadResources();

#endif //BATTLESHIP_CONSTANTS_HPP
