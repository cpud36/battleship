//
// Created by cpud36 on 11/18/16.
//

#ifndef BATTLESHIP_STATE_HPP
#define BATTLESHIP_STATE_HPP


enum class State {
  MainMenu,
  Game
};


#endif //BATTLESHIP_STATE_HPP
