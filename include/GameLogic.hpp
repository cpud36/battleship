//
// Created by cpud36 on 19.11.16.
//

#ifndef BATTLESHIP_GAMELOGIC_HPP
#define BATTLESHIP_GAMELOGIC_HPP


#include <array>
#include "Grid.hpp"
#include "PlayerTeam.hpp"

class GameLogic {
  PlayerTeam turn = PlayerTeam::First;
public:
  Grid grid1;
  Grid grid2;

  PlayerTeam getTurn() const { return turn; }

  void setTurn(PlayerTeam turn) { GameLogic::turn = turn; }

  bool isOver() const { return grid1.isDestroyed() || grid2.isDestroyed(); }

  PlayerTeam getWinner() const { return grid1.isDestroyed() ? PlayerTeam::Second : PlayerTeam::First; }

  void resetGrids();
};


#endif //BATTLESHIP_GAMELOGIC_HPP
