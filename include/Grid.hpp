//
// Created by cpud36 on 19.11.16.
//

#ifndef BATTLESHIP_GRID_HPP
#define BATTLESHIP_GRID_HPP


#include <array>
#include <SFML/System/Vector2.hpp>
#include "CellState.hpp"
#include "Constants.hpp"

class Grid {
  unsigned int largestID = 0;
  bool destroyed = false;

  std::array<bool, TOTAL_SHIP_COUNT> shipDestroyed = {{false}};

  bool checkIsDestroyed();

  int suggestID(unsigned int x, unsigned int y);

  void checkShipDestroyed(unsigned int x, unsigned int y);

  void assignShipIDs();

public:
  std::array<std::array<CellState, GRID_WIDTH>, GRID_HEIGHT> grid = {{{{CellState()}}}};

  void addShip(const int x, const int y, int value = SHIPID_UNKNOWN);

  bool validate();

  bool fire(unsigned int x, unsigned int y);

  bool isValidIndex(int x, int y) const;

  bool isDestroyed() const { return destroyed; }

  bool validateShipCount();

  void generate();

  bool isShipDestroyed(const unsigned int x, const unsigned int y) const;

  void reset();
};


#endif //BATTLESHIP_GRID_HPP
