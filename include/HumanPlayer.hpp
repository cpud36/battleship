//
// Created by cpud36 on 20.11.16.
//

#ifndef BATTLESHIP_HUMANPLAYER_HPP
#define BATTLESHIP_HUMANPLAYER_HPP


#include "PlayerController.hpp"
#include "GridRender.hpp"
#include "GridShipRender.hpp"
#include "PlayerState.hpp"
#include "GridEnemyRender.hpp"
#include "LockScreen.hpp"

class HumanPlayer : public PlayerController {
  GridShipRender grid1Render;
  GridEnemyRender grid2Render;
  LockScreen lockScreen;
public:
  PlayerState state = PlayerState::Building;

  HumanPlayer(PlayerTeam team);

  virtual void handle_event(sf::Event &event) override;

  virtual void update() override;

  virtual void resize(sf::Vector2f size) override;

  virtual void setGrids(Grid *my, Grid *opponent);

  virtual void endTurn();


};


#endif //BATTLESHIP_HUMANPLAYER_HPP
