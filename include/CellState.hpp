//
// Created by cpud36 on 19.11.16.
//

#ifndef BATTLESHIP_CELLSTATE_HPP
#define BATTLESHIP_CELLSTATE_HPP

const int SHIPID_UNKNOWN = -1;

struct CellState {
  // Whether or not the opponent tried to fire at this cell
  bool fired;
  // The ID of the ship this cell contains
  // NOTE: use SHIPID_UNKNOWN for unassigned ship, and 0 for absence of ship
  int shipID;

  // Does this cell contains a shipID and it is destroyed(fired)
  bool destroyed() const { return fired && shipID; }

  bool loosed() const { return fired && !shipID; }


  CellState(int Ship = 0, bool Fired = false) : fired(Fired), shipID(Ship) {}
};


#endif //BATTLESHIP_CELLSTATE_HPP
