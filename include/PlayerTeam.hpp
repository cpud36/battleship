//
// Created by cpud36 on 20.11.16.
//

#ifndef BATTLESHIP_PLAYERTEAM_HPP
#define BATTLESHIP_PLAYERTEAM_HPP


enum class PlayerTeam {
  First,
  Second,
};


#endif //BATTLESHIP_PLAYERTEAM_HPP
