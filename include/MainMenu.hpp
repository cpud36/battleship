//
// Created by cpud36 on 11/18/16.
//

#ifndef BATTLESHIP_MAINMENU_HPP
#define BATTLESHIP_MAINMENU_HPP

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "../GraphicsUtils/include/Button.hpp"
#include "State.hpp"

class Game;

class MainMenu {
  Game *game;
  sf::RenderTarget *window;
  Button buttonPVP;
  Button buttonPVC;

  void resize(sf::Vector2f size);
public:
  MainMenu(Game *game = nullptr, sf::RenderTarget *window = nullptr);

  void handle_event(sf::Event &event);

  void update();
};


#endif //BATTLESHIP_MAINMENU_HPP
